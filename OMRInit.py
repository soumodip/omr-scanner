import wx

# IMPORT THE GUI FILE
import GUI
import OMR_Process

class OMR_GUI(GUI.OMRScanner):

    def __init__(self, parent):
        GUI.OMRScanner.__init__(self, parent)

    def closeBtnTrigger(self, event):
        self.Close();

    def saveBtnTrigger(self, event):
        openFileDialog = wx.FileDialog(self, "OPEN RESULT(.json) FILE ", "", "","JSON Files (*.json)|*.json", wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if openFileDialog.ShowModal() == wx.ID_OK:
            OMR_Process.storeResultData(openFileDialog.GetPath())
        openFileDialog.Destroy()

    def uploadBtnTrigger(self, event):
        openFileDialog = wx.FileDialog(self, "OPEN OMR(.jpg) FILE ", "", "", "IMAGE Files (*.jpg)|*.jpg",
                                       wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if openFileDialog.ShowModal() == wx.ID_OK:
            result=OMR_Process.processImg(openFileDialog.GetPath())
            self.txtRollNumber.SetLabel("Roll Number : "+str(result[1]))
            self.txtMarks.SetLabel("Marks Obtained : " + str(result[0])+"/100")
        openFileDialog.Destroy()

if __name__ == '__main__':
    app = wx.App(False)
    frame = OMR_GUI(None)
    frame.Show(True)
    # START THE APPLICATION
    app.MainLoop()