<h2><b>OMR Scanner</b></h2>
<p><i style="font-size:18px;"><b>OMR Scanner</b></i> is a OMR sheet scaaning application made on top of Python + OpenCV, which detects corrects answers and gives a result. Scoring Pattern - 4 -> Correct Answer, 0 -> No Attempt & -1 -> Wrong Answer.
</p>
<hr/>
<h3><b>Steps</b></h3>
<ul>
	<li>
        <p>Run $ python GUI.py</p>
    </li>
    <li>
        <p>Once the OMR Scanner opens, click on the load answers and select the file : ANSWER_KEY.json to load all correct answers</p>
    </li>
    <li>
        <p>After that, click the upload OMR sheet button to upload the OMR sheet.</p>
    </li>
    <li>
        <p>Finally the app generates the result.</p>
    </li>
</ul>
<hr/>
<h3><b>Tips</b></h3>
<p>I have also provided a set up for generating .exe [ executable file ] using $ python setup.py</p>
<hr/>
<p>Developed by <a href="http://www.soumodippaul.com/">Soumodip Paul</a></p>