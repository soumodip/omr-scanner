import wx
import wx.xrc
import os


#OMR SCANNER CLASS WHICH CONTAINS THE BASIC UI OF THE APPLICATION

class OMRScanner(wx.Frame):
    def __init__(self, parent):
        wx.Frame.__init__(self, parent, id=wx.ID_ANY, title=wx.EmptyString, pos=wx.DefaultPosition,
                          size=wx.Size(500, 300), style=0 | wx.NO_BORDER | wx.TAB_TRAVERSAL)

        self.SetSizeHintsSz(wx.DefaultSize, wx.DefaultSize)
        self.SetFont(wx.Font(9, 74, 90, 90, False, "Calibri"))
        self.SetBackgroundColour(wx.Colour(208, 208, 208))

        insideBox = wx.BoxSizer(wx.VERTICAL)

        self.closeBtn = wx.BitmapButton(self, wx.ID_ANY, wx.Bitmap(os.getcwd()+"\close.png", wx.BITMAP_TYPE_ANY),
                                        wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        self.closeBtn.SetForegroundColour(wx.SystemSettings.GetColour(wx.SYS_COLOUR_WINDOW))
        self.closeBtn.SetBackgroundColour(wx.Colour(208, 208, 208))
        self.closeBtn.SetToolTipString(u"Close the OMR Scanner")

        insideBox.Add(self.closeBtn, 0, wx.ALL | wx.ALIGN_RIGHT, 5)

        self.uploadBtn = wx.BitmapButton(self, wx.ID_ANY, wx.Bitmap(os.getcwd()+"\scanner.png", wx.BITMAP_TYPE_ANY),
                                         wx.Point(-1, -1), wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        self.uploadBtn.SetBackgroundColour(wx.Colour(208, 208, 208))
        self.uploadBtn.SetToolTipString(u"Upload File and Get Result")

        insideBox.Add(self.uploadBtn, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 8)

        self.saveBtn = wx.BitmapButton(self, wx.ID_ANY, wx.Bitmap(os.getcwd()+"\store.png", wx.BITMAP_TYPE_ANY),
                                       wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW | wx.NO_BORDER)
        self.saveBtn.SetBackgroundColour(wx.Colour(208, 208, 208))
        self.saveBtn.SetToolTipString(u"Click to Save Answers")

        insideBox.Add(self.saveBtn, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 15)

        self.txtRollNumber = wx.StaticText(self, wx.ID_ANY, u"Roll Number : NAN", wx.DefaultPosition, wx.DefaultSize, 0)
        self.txtRollNumber.Wrap(-1)
        self.txtRollNumber.SetFont(wx.Font(12, 74, 90, 90, False, "Calibri"))

        insideBox.Add(self.txtRollNumber, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.ALL, 5)

        self.txtMarks = wx.StaticText(self, wx.ID_ANY, u"Marks Obtained : NAN", wx.DefaultPosition, wx.DefaultSize, 0)
        self.txtMarks.Wrap(-1)
        self.txtMarks.SetFont(wx.Font(12, 74, 90, 90, False, "Calibri"))

        insideBox.Add(self.txtMarks, 0, wx.ALL | wx.ALIGN_CENTER_HORIZONTAL, 5)

        self.SetSizer(insideBox)
        self.Layout()

        self.Centre(wx.BOTH)

        # CONNECTED EVENTS
        self.closeBtn.Bind(wx.EVT_BUTTON, self.closeBtnTrigger)
        self.uploadBtn.Bind(wx.EVT_BUTTON, self.uploadBtnTrigger)
        self.saveBtn.Bind(wx.EVT_BUTTON, self.saveBtnTrigger)

        #SET ICON
        icon = wx.EmptyIcon()
        icon.CopyFromBitmap(wx.Bitmap(os.getcwd()+"\icon.png", wx.BITMAP_TYPE_ANY))
        self.SetIcon(icon)

    def __del__(self):
        pass

    def closeBtnTrigger(self, event):
        event.Skip()

    def uploadBtnTrigger(self, event):
        event.Skip()

    def saveBtnTrigger(self, event):
        event.Skip()


