import json
import cv2
import numpy as np

#ARRAY STORING THE CORRECT ANSWERS
answerArray=[]
#ARRAY STORING THE ANSWERS OBTAINED FROM OMR SHEET
resultArray=np.zeros((25,4))
#ARRAY STORING THE ROLL NUMBER OBTAINED FROM OMR SHEET
rollNumberArray=np.zeros((2,10))
#ROLL NUMBER AND MARKS
rollNum=None
marksNum=0

#THIS FILE PROCESSES THE OMR SCANNING AND PROCESSING WORK
def storeResultData(path):
    global answerArray
    if len(path)>0:
        with open(path) as data_file:
            data = json.load(data_file)
            answerArray=data["RESULT"]

#THE WHOLE IMAGE PROCESSING IS DONE HERE
def processImg(path):
    global marksNum
    global rollNum
    image=cv2.imread(path)
    tempImage=image
    image=cv2.cvtColor(image,cv2.COLOR_BGR2GRAY)
    manupulateContoursThreshold(image,tempImage)
    calculateResult()
    return [marksNum,rollNum]


def manupulateContoursCanny(image):
    image = cv2.bilateralFilter(image, 11, 17, 17)
    cannyEdges=cv2.Canny(image,100,255)
    findContourandPerpectiveTransform(cannyEdges)


def manupulateContoursThreshold(image,tempImage):
    imageCopy=image.copy()
    image=cv2.GaussianBlur(image,(5,5),0)
    image=cv2.adaptiveThreshold(image,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,11,2)
    findContourandPerpectiveTransform(image,imageCopy,tempImage)


def findContourandPerpectiveTransform(image,imageCopy,tempImage):
    count=0;
    minContourArea=0;
    coordinateArray=[]
    contours, hierarchy = cv2.findContours(image, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    for contour in contours:
        if cv2.contourArea(contour)>50000 :
            peri = cv2.arcLength(contour, True)
            approx = cv2.approxPolyDP(contour, 0.02 * peri, True)
            if len(approx) == 4 and count==0:
                minContourArea=cv2.contourArea(contour)
            if len(approx) == 4 and cv2.contourArea(contour) <= minContourArea:
                count+=1;
                minContourArea=cv2.contourArea(contour)
                coordinateArray=[tuple(approx[0][0]),tuple(approx[1][0]),tuple(approx[2][0]),tuple(approx[3][0])]
    coordinateArray=filterCorrdinateArray(coordinateArray)
    #THE ACTUAL MANIPULATION OF THE IMAGE BEGINS HERE
    pts1 = np.float32(coordinateArray)
    pts2 = np.float32([[0, 0],[0, 600] , [454, 600] , [454, 0]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(imageCopy, M, (454, 600))
    tempImage = cv2.warpPerspective(tempImage, M, (454, 600))
    getResult(dst,tempImage)


def filterCorrdinateArray(coordinateArray):
    coordinateArrayInit=[(0,0),(0,0),(0,0),(0,0)]
    coordinateArray = sorted(coordinateArray, key=lambda array: array[0])
    if(coordinateArray[0][1] < coordinateArray[1][1]):
        coordinateArrayInit[0]=coordinateArray[0]
        coordinateArrayInit[1] = coordinateArray[1]
    else:
        coordinateArrayInit[0] = coordinateArray[1]
        coordinateArrayInit[1] = coordinateArray[0]
    if (coordinateArray[3][1] < coordinateArray[2][1]):
        coordinateArrayInit[3] = coordinateArray[3]
        coordinateArrayInit[2] = coordinateArray[2]
    else:
        coordinateArrayInit[2] = coordinateArray[3]
        coordinateArrayInit[3] = coordinateArray[2]
    return coordinateArrayInit


def getResult(image,tempImage):
    hsv = cv2.cvtColor(tempImage, cv2.COLOR_BGR2HSV)
    lower_black = np.array([0, 0, 0])
    upper_black = np.array([180, 255, 100])
    image = cv2.inRange(hsv, lower_black, upper_black)
    findRollNumber(image)
    imgCopy=image.copy()
    rowArray=[190,221,252,283,314,345,376,407,436,466,497,527,557,587]
    for i in range(1,26):
        if i%2==1:
            pts1 = np.float32([[65, rowArray[(i-1)/2]], [65, rowArray[(i-1)/2+1]], [195, rowArray[(i-1)/2+1]], [195, rowArray[(i-1)/2]]])
        else:
            pts1 = np.float32(
                [[298, rowArray[(i - 1) / 2]], [298, rowArray[(i - 1) / 2 + 1]], [428, rowArray[(i - 1) / 2 + 1]],
                 [428, rowArray[(i - 1) / 2]]])
        pts2 = np.float32([[0, 0], [0, 31], [130, 31], [130, 0]])
        M = cv2.getPerspectiveTransform(pts1, pts2)
        dst = cv2.warpPerspective(imgCopy, M, (130, 31))
        score(dst,(i-1))


def findRollNumber(image):
    mask = image
    pts1 = np.float32([[185,70],[185,95],[442,95],[442,70]])
    pts2 = np.float32([[0, 0], [0, 25], [200, 25], [200, 0]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(mask, M, (200, 25))
    for i in range(0,10):
        dstTemp=dst[0:25,i*20:(i+1)*20]
        if np.sum(dstTemp) >= 40000:
            rollNumberArray[0][i]= 1
        else:
            rollNumberArray[0][i] = 0
    pts1 = np.float32([[185, 95], [185, 122], [442, 122], [442, 95]])
    pts2 = np.float32([[0, 0], [0, 25], [200, 25], [200, 0]])
    M = cv2.getPerspectiveTransform(pts1, pts2)
    dst = cv2.warpPerspective(mask, M, (200, 25))
    for i in range(0, 10):
        dstTemp = dst[0:25, i * 20:(i + 1) * 20]
        if np.sum(dstTemp) >= 40000:
            rollNumberArray[1][i] = 1
        else:
            rollNumberArray[1][i] = 0

def score(image,row):
    #CHECK FOR FIRST OPTION
    imgCheck = image[0:31, 0:35]
    resultArray[row][0]= findSum(imgCheck)
    imgCheck = image[0:31, 35:65]
    resultArray[row][1]= findSum(imgCheck)
    imgCheck = image[0:31, 65:95]
    resultArray[row][2]= findSum(imgCheck)
    imgCheck = image[0:31, 95:130]
    resultArray[row][3] = findSum(imgCheck)


def findSum(image):
    sum=np.sum(image)
    if sum >= 60000 :
        return 1
    else:
        return 0
#END OF THE IMAGE PROCESSING STUFF

#SAVE THE RESULT
def calculateResult():
    global answerArray
    global marksNum
    global rollNum
    rollNum=None
    marksNum=0
    for i in range(0,25):
        index=np.where(resultArray[i]==1)
        print index[0]
        if len(index[0]) == 0:
            marksNum=marksNum+0
        else:
            if (index[0][0]+1) == answerArray[i]["ANSWER"] :
                marksNum=marksNum+4
            else:
                marksNum=marksNum-1
    #CALCULATE THE ROLL NUMBER
    index = np.where(rollNumberArray[0] == 1)
    if len(index[0]) == 0:
        rollNum="A0"
    else:
        rollNum = "A"+str(index[0][0])
    index = np.where(rollNumberArray[1] == 1)
    if len(index[0]) == 0:
        rollNum += "B0"
    else:
        rollNum += "B" + str(index[0][0])
#SAVE THE RESULT



